#Robert Kwiatkowski 10.11.2018
#Algorytmy i Struktury Danych
#Prosta Zmiana (ShakeSort)
def ShakeSort(alist):
    def zmiana(i, j):
        alist[i], alist[j] = alist[j], alist[i]
    gora = len(alist) -1
    dol = 0

    brak_zmiany = False
    while(not brak_zmiany and gora - dol > 1):
        brak_zmiany = True
        for j in range(dol, gora): #analogia do kartki przebieg do góry
            if alist[j+1] < alist[j]:
                zmiana(j+1,j)
                brak_zmiany = False
        gora = gora -1 #poprzez zmniejszanie gornej granicy w momencie gora - dol = 1 przechodzimy do przebiegu z gory do dolu

        for j in range(gora, dol, -1): #analogia do kartki przebieg do dołu
            if alist[j-1] > alist[j]:
                zmiana(j -1, j)
                brak_zmiany = False
        dol = dol + 1 #poprzez zwiekszanie dolnej granicy, w momencie gora - dol = 1 konczymy sortowanie

alist = [1, 3, 10, 2, 5, 4, 6, 11, 9]
ShakeSort(alist)
print(alist)


#zamiast uzywac pseudokodu temp=alist[j], alist[j]=alist[j-1], alist[j-1]=temp k=j, uproscilem to do funkcji zmiana