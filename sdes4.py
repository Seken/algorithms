KeyLength = 10
SubKeyLength = 8
DataLength = 8
FLength = 4

# Tablice do pierwszej i ostatniej permutationutacji
IPtable = (2, 6, 3, 1, 4, 8, 5, 7)
FPtable = (4, 1, 3, 5, 7, 2, 8, 6)

# Tables do generowania kluczy
P10table = (3, 5, 2, 7, 4, 10, 1, 9, 8, 6)
P8table = (6, 3, 7, 4, 8, 5, 10, 9)

# Tablice do funkcji funkcjaka
EPtable = (4, 1, 2, 3, 2, 3, 4, 1)
S0table = (1, 0, 3, 2, 3, 2, 1, 0, 0, 2, 1, 3, 3, 1, 3, 2)
S1table = (0, 1, 2, 3, 2, 0, 1, 3, 3, 0, 1, 0, 2, 1, 0, 3)
P4table = (2, 4, 3, 1)


def permutation(inputByte, permutationTable):
    """Permute input byte according to permutationutation table"""
    outputByte = 0
    for index, elem in enumerate(permutationTable):
        if index >= elem:
            outputByte |= (inputByte & (128 >> (elem - 1))) >> (index - (elem - 1)) #var |= value is short for var = var | value
        else:
            outputByte |= (inputByte & (128 >> (elem - 1))) << ((elem - 1) - index)  # | bitwise or operator
    print(outputByte, "xxxx")
    return outputByte


def ip(inputByte):
    return permutation(inputByte, IPtable)


def fp(inputByte):
    """Perform the final permutationutation on data"""
    return permutation(inputByte, FPtable)


def swapp(inputByte):
    """Swap the two nibbles of data"""
    return (inputByte << 4 | inputByte >> 4) & 0xff


def keyGen(key):

    def leftShift(keyBitList):
        """Perform a circular left shift on the first and second five bits"""
        shiftedKey = [None] * KeyLength
        shiftedKey[0:9] = keyBitList[1:10]
        shiftedKey[4] = keyBitList[0]
        shiftedKey[9] = keyBitList[5]
        return shiftedKey

    #  Zamiana inputu (int) na liste znakow binarnych
    keyList = [(key & 1 << i) >> i for i in reversed(range(KeyLength))]
    permutationKeyList = [None] * KeyLength
    for index, elem in enumerate(P10table):
        permutationKeyList[index] = keyList[elem - 1]
    shiftedOnceKey = leftShift(permutationKeyList)
    shiftedTwiceKey = leftShift(leftShift(shiftedOnceKey))
    subKey1 = subKey2 = 0
    for index, elem in enumerate(P8table):
        subKey1 += (128 >> index) * shiftedOnceKey[elem - 1]
        subKey2 += (128 >> index) * shiftedTwiceKey[elem - 1]
    key1 = str(bin(subKey1))
    key2 = str(bin(subKey2))
    #print("Klucz pierwszy:" + bin(subKey1))

    print("Klucz pierwszy:" + key1[2:].zfill(8))
    print("Klucz drugi:" + key2[2:].zfill(8))
    return (subKey1,subKey2)



def funkcjaka(subKey, UserInput):

    def F(sKey, rightNibble):
        aux = sKey ^ permutation(swapp(rightNibble), EPtable)
        index1 = ((aux & 0x80) >> 4) + ((aux & 0x40) >> 5) + \
                 ((aux & 0x20) >> 5) + ((aux & 0x10) >> 2)
        index2 = ((aux & 0x08) >> 0) + ((aux & 0x04) >> 1) + \
                 ((aux & 0x02) >> 1) + ((aux & 0x01) << 2)
        sboxOutputs = swapp((S0table[index1] << 2) + S1table[index2])
        return permutation(sboxOutputs, P4table)

    leftNibble, rightNibble = UserInput & 0xf0, UserInput & 0x0f
    return (leftNibble ^ F(subKey, rightNibble)) | rightNibble


def encrypt(key, plaintext):
    data = funkcjaka(keyGen(key)[0], ip(plaintext))
    print("po rundzie pierwszej:" + hex(data))
    print("po rundzie drugiej:" + hex(funkcjaka(keyGen(key)[1], swapp(data))))
    #print(hex(fp(funkcjaka(keyGen(key)[1], swapp(data)))))
    # g11 =len(hex(fp(funkcjaka(keyGen(key)[0], swapp(data)))))
    # if len(hex(fp(funkcjaka(keyGen(key)[0], swapp(data))))) == 4:
    #     print(str())
    #     mmm = str(hex(fp(funkcjaka(keyGen(key)[0], swapp(data)))))
    #     mmmm = "w" + mmm[3:]
    #     print(mmmm)
    #     print("Wynik szyfrowania bajtu to " + mmmm)
    #     return fp(funkcjaka(keyGen(key)[0], swapp(data)))
    print ("Wynik szyfrowania bajtu to " +hex(fp(funkcjaka(keyGen(key)[1], swapp(data)))))
    return fp(funkcjaka(keyGen(key)[1], swapp(data)))


def decrypt(key, ciphertext):
    """Decrypt encryptet hexadecimal number with key"""
    data = funkcjaka(keyGen(key)[1], ip(ciphertext))
    if len(hex(fp(funkcjaka(keyGen(key)[0], swapp(data)))))%2 != 0:
        print("Wynik odszyfrowania bajtu to w" + hex(fp(funkcjaka(keyGen(key)[0], swapp(data)))))
        return fp(funkcjaka(keyGen(key)[0], swapp(data)))
    print("Wynik odszyfrowania bajtu to 0" +hex(fp(funkcjaka(keyGen(key)[0], swapp(data)))))
    return fp(funkcjaka(keyGen(key)[0], swapp(data)))


#0xff if byte1 is an 8-bit integer type then it's pointless - if it is more than 8 bits it will essentially give you the last 8 bits of the valu
#0xf0 is to ignore the first 4 left bites

szyfr = input("Wpisz ciag znakow hex (juz bez 0x)")
if len(szyfr)%2 != 0: #PIERWSZA CZESC ZADANIA, DOPISANIE ZERA
    szyfr = szyfr+"0"
##############116-150 skomentowane na potrzeby testu
szyfr_string = str(szyfr)
print(szyfr_string)
info = [szyfr_string[i:i+2] for i in range(0, len(szyfr_string), 2)]
info2 = [hex(int(info[i],base=16)) for i in range(0,len(info))]


info3 = []
for x in range(0,len(info2)):
    info3.append(encrypt(0b1100000011,int(info2[x],base=16)))
info4 = [hex(info3[i]) for i in range(0,len(info3))]
print(info4)
info5 = [str(info4[i]) for i in range(0, len(info4))]
s = ""
g=s.join(info5)
t = [g[i:i+2] for i in range(0, len(g), 2)]
b=t[1::2]
c = ""
iks = c.join(b) ###to jest string!!
print("WYNIK OSTATECZNY TO 0x"+iks)





print("##################################################################")
igrek = []
info10 = [iks[i:i+2] for i in range(0, len(iks), 2)]
info20 = [hex(int(info10[i],base=16)) for i in range(0,len(info10))]
for x in range(0,len(info20)):
    igrek.append(decrypt(0b1100000011,int(info20[x],base=16)))



info100 = [hex(igrek[i]) for i in range(0,len(igrek))]
print(info100)

info500 = [str(info100[i]) for i in range(0, len(info100))]
s0 = ""
g0=s0.join(info500)
t0 = [g0[i:i+2] for i in range(0, len(g0), 2)]
b0=t0[1::2]
c0 = ""
iks0 = c0.join(b0) ###to jest string!!
print("WYNIK DESZYFROWANIA TO 0x"+iks0)

#encrypt(0b1100000011,int(szyfr, base=16))
#keyGen(0b1100000011)
#print(bin(162))
#print(bin(15).zfill(8))
#print("wynik szyfrowania:" + hex(encrypt(0b1100000011,0xc3)))
#decrypt(0b1100000011,0xef)

#86!!